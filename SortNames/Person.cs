﻿using System;

namespace SortNames
{
    public class Person : IComparable<Person>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
        public override string ToString()
        {
            return LastName + ", " + FirstName;
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, null))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (GetType() != obj.GetType())
                return false;
            Person other = (Person)obj;
            return FirstName.Equals(other.FirstName) && LastName.Equals(other.LastName);
        }
        public override int GetHashCode()
        {
            return new { FirstName, LastName }.GetHashCode();
        }
        public int CompareTo(Person other)
        {
            int lastNameComparison = LastName.CompareTo(other.LastName);
            // If same last names then compare first names
            if (lastNameComparison == 0)
                return FirstName.CompareTo(other.FirstName);
            else
                return lastNameComparison;
        }
    }
}