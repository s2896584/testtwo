﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SortNames
{
    public class SortNames
    {
        public static int Main(string[] args)
        {
            if (args == null || args.Length == 0 || args[0].Equals(""))
            {
                Console.WriteLine("Invalid usage: Enter path to file.");
                return 1;
            }
            string inputPath = args[0];
            // Only want to work with absolute paths.
            if (Uri.IsWellFormedUriString(inputPath, UriKind.Relative))
            {
                inputPath = Path.GetFullPath(inputPath);
            }
            List<Person> personList = PersonHelper.ReadFromFile(inputPath);
            if (personList == null || personList.Count == 0)
            {
                return 1;
            }
            personList.Sort();
            string outputPath = Path.GetDirectoryName(inputPath) +
                "\\" + Path.GetFileNameWithoutExtension(inputPath) + "-sorted" + Path.GetExtension(inputPath);
            PersonHelper.WriteToFile(outputPath, personList);
            personList.ForEach(p => Console.WriteLine(p.ToString()));
            Console.WriteLine("Finished: created " + Path.GetFileName(outputPath));
            return 0;
        }
    }
}