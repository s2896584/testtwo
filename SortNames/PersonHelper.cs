﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SortNames
{
    public static class PersonHelper
    {
        public static List<Person> ReadFromFile(string path)
        {
            if (!File.Exists(path))
            {
                Console.WriteLine("File not found.");
                return null;
            }
            List<Person> personList = new List<Person>();
            foreach (string line in File.ReadLines(path))
            {
                string[] tokens = line.Split(',');
                if (tokens.Length != 2)
                {
                    Console.WriteLine("Improperly formatted line - Supported format: FirstName, LastName.");
                    break;
                }
                Person p = new Person(tokens[1].Trim(), tokens[0].Trim());
                personList.Add(p);
            }
            if (personList.Count == 0)
            {
                Console.WriteLine("Empty file entered");
            }
            return personList;
        }
        public static void WriteToFile(string path, List<Person> personList)
        {
            try
            {
                using (StreamWriter sw = File.CreateText(path))
                {
                    foreach (Person p in personList)
                    {
                        sw.WriteLine(p.ToString());
                    }
                }
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("Not authorized to write to file");
                Environment.Exit(1);
            }
        }
    }
}