﻿using Xunit;

namespace SortNames.Tests
{
    public class PersonTests
    {
        [Fact]
        public void ConstructorTest()
        {
            Person p = new Person("Abraham", "Lincoln");
            Assert.Equal(p.FirstName, "Abraham");
            Assert.Equal(p.LastName, "Lincoln");
        }
        [Fact]
        public void ToStringTest()
        {
            Person p = new Person("Abraham", "Lincoln");
            Assert.Equal(p.ToString(), "Lincoln, Abraham");
        }
        [Fact]
        public void CompareToTest()
        {
            Person p1 = new Person("Theodore", "Roosevelt");
            Person p2 = new Person("John", "Adams");
            Person p3 = new Person("Franklin", "Roosevelt");
            int expected = 1;
            int actual = p1.CompareTo(p2);
            Assert.Equal(expected, actual);
            actual = p1.CompareTo(p3);
            Assert.Equal(expected, actual);
        }
    }
}