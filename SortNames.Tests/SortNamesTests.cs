﻿using Xunit;

namespace SortNames.Tests
{
    public class SortNamesTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public void ReadFromFileWithInvalidArgumentsTest(string input)
        {
            int expected = 1;
            int actual = SortNames.Main(new string[] { input });
            Assert.True(expected == actual);
        }
    }
}