﻿using System.Collections.Generic;
using Xunit;

namespace SortNames.Tests
{
    public class PersonHelperTests
    {
        [Fact]
        public void ReadFromFileTest()
        {
            List<Person> expected = new List<Person>
            {
                new Person("THEODORE", "BAKER"),
                new Person("ANDREW", "SMITH"),
                new Person("MADISON", "KENT"),
                new Person("FREDRICK", "SMITH")
            };
            List<Person> actual = PersonHelper.ReadFromFile(@"non existant.txt");
            Assert.Equal(null, actual);
            actual = PersonHelper.ReadFromFile(@"..\..\..\names.txt");
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void WriteToFileTest()
        {
            List<Person> expected = PersonHelper.ReadFromFile(@"..\..\..\names.txt");
            expected.Sort();
            PersonHelper.WriteToFile(@"..\..\..\names-sorted.txt", expected);
            List<Person> actual = PersonHelper.ReadFromFile(@"..\..\..\names-sorted.txt");
            Assert.Equal(expected, actual);
        }
    }
}